package com.shahin.fmcountries.domain.utils
//to map entity & DTO to and from DomainModel
interface DomainMapper<T,DomainModel> {

    fun mapToDomainModel(model: T):DomainModel

    fun mapFromDomainModel(domainModel: DomainModel):T


}