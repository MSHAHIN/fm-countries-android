package com.shahin.fmcountries.domain.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Country(
    val capital:List<String> = listOf(),
    val languages: HashMap<String?,String?> = hashMapOf(),
    val population:Int? = null,
    val name: Name? = null
) : Parcelable

@Parcelize
data class Name(
    val common:String? = null
) : Parcelable

