package com.shahin.fmcountries.presentation.ui.countries_list

sealed class CountriesListEvent {
    object OnNewSearchEvent:CountriesListEvent()
}