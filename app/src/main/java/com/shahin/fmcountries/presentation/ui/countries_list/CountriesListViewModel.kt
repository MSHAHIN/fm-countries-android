package com.shahin.fmcountries.presentation.ui.countries_list

import android.util.Log
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.shahin.fmcountries.domain.model.Address
import com.shahin.fmcountries.domain.model.Country
import com.shahin.fmcountries.presentation.ui.countries_list.CountriesListEvent.OnNewSearchEvent
import com.shahin.fmcountries.repository.AddressRepository
import com.shahin.fmcountries.repository.CountryRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject


const val TAG = "Countries List VM"

@HiltViewModel
class CountriesListViewModel
@Inject
constructor(
    private val addressRepository: AddressRepository,
    private val countryRepository: CountryRepository,
    ) :ViewModel() {
    val countries: MutableState<List<String>> = mutableStateOf(listOf())
    val countryModels: MutableState<List<Country>> = mutableStateOf(listOf())
    val query = mutableStateOf("")
    val loading = mutableStateOf(false)
    private val toastMessageObserver: MutableLiveData<String?> = MutableLiveData<String?>()

    //Event-driven method to support unidirectional data flow
    fun onEventTrigger(event: CountriesListEvent){
        viewModelScope.launch {
            try {
                when(event){
                    //On hitting search action
                    is OnNewSearchEvent ->{
                        if(!loading.value) {
                            val count: Int? = checkAndGetQuery()
                            if (count != null && count in 5..20) {
                                newSearch(count, true)
                            } else {
                                clearQuery()
                                toastMessageObserver.setValue("Invalid input. Please enter a number between 5 and 20")
                            }
                        }
                        else{
                            toastMessageObserver.setValue("Please wait while the results are loading")
                        }

                    }
                }
            }
            catch (e:java.lang.Exception){
                Log.e(TAG, "onTriggerEvent: Exception:${e}",e.cause)
            }
        }
    }

    //checking if proper input is entered
     fun checkAndGetQuery():Int?{
        var count:Int? = null
        try {
            count  = Integer.parseInt(query.value)
        }catch (e:java.lang.NumberFormatException){
            e.printStackTrace()
        }
        catch (e:Exception){
            e.printStackTrace()
        }
        return count
    }


    fun getToastObserver(): LiveData<String?> {
        return toastMessageObserver
    }

    //fetch addresses
    private suspend fun newSearch(count: Int, resetSearch:Boolean) {
        loading.value = true
        if(resetSearch)
            resetSearchState()
        while(countries.value.size<count){
            var result : Address? = null
            try {
                result = addressRepository.getAddress()
            }
            catch (e:IOException){
                e.printStackTrace()
            }
            catch (e:HttpException){
                e.printStackTrace()
            }
            catch (e:Exception){
                e.printStackTrace()
            }
            //add country names to countries list, call country api for each country name
            if(result!=null){
                if(!countries.value.contains(result.country!!)){
                    appendCountryName(result.country!!)
                    getCountryDetails(result.country!!,count)
                }
            }
        }
        loading.value = false
    }



    /*call country api for each country name. If search fails for one country name,
    remove that country name from countries list and call search again, so that the erroneous country name
    will be replaced*/
    private suspend fun getCountryDetails(country: String,count:Int) {

        var result : Country? = null
        try {
            result = countryRepository.getCountry(country)
        }
        catch (e:IOException){
            e.printStackTrace()
        }
        catch (e:HttpException){
            e.printStackTrace()
        }
        catch (e:Exception){
            e.printStackTrace()
        }
        if(result!=null){
            appendCountryDetails(result)
        }
        else{
            removeCountryName(country)
            newSearch(count,false)
        }
    }

    private fun resetSearchState(){
        countries.value = listOf()
        countryModels.value = listOf()
        clearQuery()
    }

    private fun clearQuery(){
        query.value =""
    }

    fun appendCountryName(newCountry:String )
    {
        val current = ArrayList(countries.value)
        current.add(newCountry)
        this.countries.value = current
    }

    fun removeCountryName(countryName:String )
    {
        val current = ArrayList(countries.value)
        current.remove(countryName)
        this.countries.value = current
    }
    fun appendCountryDetails(newCountry:Country )
    {
        val current = ArrayList(countryModels.value)
        current.add(newCountry)
        this.countryModels.value = current
    }

    fun onQueryChanged(query: String){
        setQuery(query)
    }

    private fun setQuery(query:String){
        this.query.value = query
    }
}