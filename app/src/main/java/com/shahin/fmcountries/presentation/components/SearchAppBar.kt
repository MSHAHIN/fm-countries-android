package com.shahin.fmcountries.presentation.components

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Search
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp

@OptIn(ExperimentalMaterial3Api::class, ExperimentalComposeUiApi::class)
@Composable
fun SearchAppBar (
    query:String,
    onQueryChanged:(String)->Unit,
    onExecuteSearch:()->Unit,
){
    val keyboardController = LocalSoftwareKeyboardController.current

    Surface(
        modifier = Modifier
            .fillMaxWidth(),
        color = MaterialTheme.colorScheme.surface,
        shadowElevation = 8.dp
    ) {
        TextField(
            modifier = Modifier
                .fillMaxWidth(0.9f)
                .padding(8.dp)
                .background(MaterialTheme.colorScheme.surface),
            label = {
                Text(text = "Search")
            },
            keyboardOptions = KeyboardOptions(
                keyboardType = KeyboardType.Number,
                imeAction = ImeAction.Search
            ),
            keyboardActions = KeyboardActions(
                onSearch = {
                    onExecuteSearch()
                    keyboardController?.hide()
                },
            ),
            leadingIcon = {
                Icon(Icons.Filled.Search, contentDescription = "Search")
            },
            value = query, onValueChange = { text ->
                onQueryChanged(text)
            },
            textStyle = TextStyle(
                color = MaterialTheme.colorScheme.onSurface
            )
        )
    }
}