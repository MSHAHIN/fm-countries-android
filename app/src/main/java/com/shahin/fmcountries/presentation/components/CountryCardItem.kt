package com.shahin.fmcountries.presentation.components

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import com.shahin.fmcountries.domain.model.Country
const val NO_INFO_TEXT = "No information Found!"
@Composable
fun CountryCardItem(index:Int, item: Country) {
    Card(
        shape = MaterialTheme.shapes.small,
        elevation = CardDefaults.cardElevation(defaultElevation = 20.dp),
        modifier = Modifier
            .padding(
                bottom = 6.dp,
                top = 6.dp
            )
            .clickable(onClick = { }),
    ) {

        Row(verticalAlignment = Alignment.CenterVertically) {
            Text(
                (index + 1).toString(),
                modifier = Modifier.padding(start = 8.dp)
            )
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(
                        top = 12.dp,
                        bottom = 12.dp,
                        start = 8.dp,
                        end = 8.dp
                    )
            ) {
                Text(
                    text = "Country: ${
                        if(item.name?.common!!.isEmpty()){
                            NO_INFO_TEXT
                        }
                        else{
                            item.name.common
                        }
                    }",
                    fontWeight = FontWeight.Bold
                )
                Text(text = "Capital: ${
                    if(item.capital.isEmpty()){
                        NO_INFO_TEXT
                    }
                    else{
                        item.capital.joinToString(", ")
                    }
                }")
                Text(text = "Population: ${
                    item.population.toString().ifEmpty {
                        NO_INFO_TEXT
                    }
                }")
                Text(
                    text = "Languages: ${
                        if(item.languages.isEmpty()){
                            NO_INFO_TEXT
                        }
                        else{
                            item.languages.values.joinToString(", ")
                        }
                    }"
                )
            }
        }
    }
}