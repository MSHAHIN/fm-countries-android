package com.shahin.fmcountries.presentation.ui.countries_list

import CircularIndeterminateProgressBar
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material3.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.unit.dp
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.shahin.fmcountries.presentation.components.CountryCardItem
import com.shahin.fmcountries.presentation.components.SearchAppBar
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class CountriesListFragment : Fragment() {
    @OptIn(ExperimentalMaterial3Api::class)
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val viewModel: CountriesListViewModel by activityViewModels()
        val lifecycleOwner = viewLifecycleOwner
        viewModel.getToastObserver().observe(lifecycleOwner) { message: String? ->
            Toast.makeText(
                activity,
                message,
                Toast.LENGTH_SHORT
            ).show()
        }

        return ComposeView(requireContext()).apply {
            setContent {
                val countries = viewModel.countryModels.value
                val query = viewModel.query.value
                val isLoading = viewModel.loading.value

                Scaffold(
                    topBar = {
                        SearchAppBar(query = query,
                            onQueryChanged = viewModel::onQueryChanged,
                            onExecuteSearch = { viewModel.onEventTrigger(CountriesListEvent.OnNewSearchEvent) })
                    }
                ) {
                    Column(modifier = Modifier.padding(it)) {
                        if (!isLoading)
                            LazyColumn(
                                modifier = Modifier
                                    .padding(horizontal = 16.dp)
                                    .fillMaxSize()
                            ) {
                                itemsIndexed(items = countries) { index, item ->
                                    CountryCardItem(index,item)
                                }
                            }
                    }
                    CircularIndeterminateProgressBar(isDisplayed = isLoading)
                }
            }
        }
    }
}