import androidx.compose.foundation.layout.*
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.layoutId
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.constraintlayout.compose.ConstraintSet

@Composable
fun CircularIndeterminateProgressBar(
  isDisplayed: Boolean
){
    if(isDisplayed){


        BoxWithConstraints(
            modifier = Modifier.fillMaxSize()
        ) {
            val constraints = if(minWidth<600.dp) {
                myDecoupledConstraints(0.3f)
            }
            else{
                myDecoupledConstraints(0.7f)
            }
            ConstraintLayout(
                constraintSet = constraints,
                modifier = Modifier.fillMaxSize()
            ) {
                CircularProgressIndicator(
                    modifier = Modifier.layoutId("progressBar"),
                    color = MaterialTheme.colorScheme.primary
                )
                Text(text = "Loading...",
                    style = MaterialTheme.typography.bodyMedium,
                    modifier = Modifier.layoutId("text"),)
            }
        }

    }
}

fun myDecoupledConstraints(verticalBias: Float): ConstraintSet {
    return ConstraintSet{
        val guideline = createGuidelineFromTop(verticalBias)
        val progressBar = createRefFor("progressBar")
        val text = createRefFor("text")
        constrain(progressBar){
            top.linkTo(guideline)
            start.linkTo(parent.start)
            end.linkTo(parent.end)
        }

        constrain(text){
            top.linkTo(progressBar.bottom)
            start.linkTo(parent.start)
            end.linkTo(parent.end)
        }
    }
}