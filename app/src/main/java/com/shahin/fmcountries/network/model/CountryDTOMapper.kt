package com.shahin.fmcountries.network.model

import com.shahin.fmcountries.domain.model.Country
import com.shahin.fmcountries.domain.model.Name
import com.shahin.fmcountries.domain.utils.DomainMapper

class CountryDTOMapper: DomainMapper<CountryDTO, Country> {
    override fun mapToDomainModel(model: CountryDTO): Country {
        return Country(
            capital = model.capital,
            languages = model.languages,
            population = model.population,
            name = Name(common = model.name?.common)
        )
    }

    override fun mapFromDomainModel(domainModel: Country): CountryDTO {
        return CountryDTO(
            capital = domainModel.capital,
            languages = domainModel.languages,
            population = domainModel.population,
            name = NameDTO(common = domainModel.name?.common)
        )
    }


    fun toDomainList(initial: List<CountryDTO>):List<Country>{
        return initial.map { mapToDomainModel(it) }
    }


}