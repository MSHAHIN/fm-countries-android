package com.shahin.fmcountries.network

import com.shahin.fmcountries.network.model.CountryDTO
import retrofit2.http.GET
import retrofit2.http.Path


interface CountryService {
    @GET("name/{country}")
    suspend fun get(@Path("country") name:String): List<CountryDTO>
}