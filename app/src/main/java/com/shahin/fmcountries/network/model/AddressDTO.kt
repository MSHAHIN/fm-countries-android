package com.shahin.fmcountries.network.model

import com.google.gson.annotations.SerializedName

data class AddressDTO(
    @SerializedName("id")
    val id:Int? = null,

    @SerializedName("uid")
    val uid:String?= null,

    @SerializedName("city")
    val city:String?= null,

    @SerializedName("street_name")
    val streetName:String?= null,

    @SerializedName("street_address")
    val streetAddress:String?= null,

    @SerializedName("secondary_address")
    val secondaryAddress:String?= null,

    @SerializedName("building_number")
    val buildingNumber:String?= null,

    @SerializedName("mail_box")
    val mailBox:String?= null,

    @SerializedName("community")
    val community:String?= null,

    @SerializedName("zip_code")
    val zipCode:String?= null,

    @SerializedName("zip")
    val zip:String?= null,

    @SerializedName("postcode")
    val postcode:String?= null,


    @SerializedName("time_zone")
    val timeZone:String?= null,


    @SerializedName("street_suffix")
    val streetSuffix:String?= null,

    @SerializedName("city_suffix")
    val citySuffix:String?= null,


    @SerializedName("city_prefix")
    val cityPrefix:String?= null,


    @SerializedName("state")
    val state:String?= null,


    @SerializedName("state_abbr")
    val stateAbbr:String?= null,

    @SerializedName("country")
    val country:String?= null,

    @SerializedName("country_code")
    val countryCode:String?= null,

    @SerializedName("latitude")
    val latitude:Double?= null,

    @SerializedName("longitude")
    val longitude:Double?= null,

    @SerializedName("full_address")
    val fullAddress:String?= null
)