package com.shahin.fmcountries.network.model

import com.google.gson.annotations.SerializedName

data class CountryDTO(

    @SerializedName("capital")
    val capital:List<String> = listOf(),

    @SerializedName("languages")
    val languages: HashMap<String?,String?> = hashMapOf(),

    @SerializedName("population")
    val population:Int? = null,

    @SerializedName("name")
    val name: NameDTO? = null
)

data class NameDTO(

    @SerializedName("common")
    val common: String? = null
)