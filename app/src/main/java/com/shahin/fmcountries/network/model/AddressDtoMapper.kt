package com.shahin.fmcountries.network.model

import com.shahin.fmcountries.domain.model.Address
import com.shahin.fmcountries.domain.utils.DomainMapper

class AddressDtoMapper: DomainMapper<AddressDTO, Address> {
    override fun mapToDomainModel(model: AddressDTO): Address {
        return Address(
            id = model.id,
            uid = model.uid,
            city = model.city,
            streetName = model.streetName,
            streetAddress  = model.streetAddress,
            secondaryAddress = model.secondaryAddress,
            buildingNumber = model.buildingNumber,
            mailBox = model.mailBox,
            community = model.community,
            zipCode = model.zipCode,
            zip = model.zip,
            postcode = model.postcode,
            timeZone = model.timeZone,
            streetSuffix = model.streetSuffix,
            citySuffix = model.citySuffix,
            cityPrefix = model.cityPrefix,
            state = model.state,
            stateAbbr = model.stateAbbr,
            country = model.country,
            countryCode = model.countryCode,
            latitude = model.latitude,
            longitude = model.longitude,
            fullAddress = model.fullAddress,
        )
    }

    override fun mapFromDomainModel(domainModel: Address): AddressDTO {
        return AddressDTO(
            id = domainModel.id,
            uid = domainModel.uid,
            city = domainModel.city,
            streetName = domainModel.streetName,
            streetAddress  = domainModel.streetAddress,
            secondaryAddress = domainModel.secondaryAddress,
            buildingNumber = domainModel.buildingNumber,
            mailBox = domainModel.mailBox,
            community = domainModel.community,
            zipCode = domainModel.zipCode,
            zip = domainModel.zip,
            postcode = domainModel.postcode,
            timeZone = domainModel.timeZone,
            streetSuffix = domainModel.streetSuffix,
            citySuffix = domainModel.citySuffix,
            cityPrefix = domainModel.cityPrefix,
            state = domainModel.state,
            stateAbbr = domainModel.stateAbbr,
            country = domainModel.country,
            countryCode = domainModel.countryCode,
            latitude = domainModel.latitude,
            longitude = domainModel.longitude,
            fullAddress = domainModel.fullAddress
        )
    }

}