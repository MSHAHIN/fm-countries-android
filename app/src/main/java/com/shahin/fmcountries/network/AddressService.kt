package com.shahin.fmcountries.network

import com.shahin.fmcountries.network.model.AddressDTO
import retrofit2.http.GET

interface AddressService {

    @GET("addresses")
    suspend fun get(): AddressDTO
}