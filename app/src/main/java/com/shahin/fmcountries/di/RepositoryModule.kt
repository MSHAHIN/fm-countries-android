package com.shahin.fmcountries.di

import com.shahin.fmcountries.network.AddressService
import com.shahin.fmcountries.network.CountryService
import com.shahin.fmcountries.network.model.AddressDtoMapper
import com.shahin.fmcountries.network.model.CountryDTOMapper
import com.shahin.fmcountries.repository.AddressRepository
import com.shahin.fmcountries.repository.AddressRepositoryImplementation
import com.shahin.fmcountries.repository.CountryRepository
import com.shahin.fmcountries.repository.CountryRepositoryImplementation
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RepositoryModule {

    @Singleton
    @Provides
    fun provideAddressRepository(
        addressDtoMapper: AddressDtoMapper,
        addressService: AddressService
    ):AddressRepository{
        return AddressRepositoryImplementation(addressService, addressDtoMapper)
    }

    @Singleton
    @Provides
    fun provideCountryRepository(
        countryDtoMapper: CountryDTOMapper,
        countryService: CountryService
    ):CountryRepository{
        return CountryRepositoryImplementation(countryService, countryDtoMapper)
    }

}