package com.shahin.fmcountries.di

import com.google.gson.GsonBuilder
import com.shahin.fmcountries.network.AddressService
import com.shahin.fmcountries.network.CountryService
import com.shahin.fmcountries.network.model.AddressDtoMapper
import com.shahin.fmcountries.network.model.CountryDTOMapper
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

    val interceptor : HttpLoggingInterceptor = HttpLoggingInterceptor().apply {
        level = HttpLoggingInterceptor.Level.BODY
    }

    val client : OkHttpClient = OkHttpClient.Builder().apply {
        addInterceptor(interceptor)
        .readTimeout(60, TimeUnit.SECONDS)
        .connectTimeout(60, TimeUnit.SECONDS)
        .writeTimeout(60, TimeUnit.SECONDS)
    }.build()


    @Singleton
    @Provides
    fun provideAddressMapper(): AddressDtoMapper {
        return AddressDtoMapper()
    }

    @Singleton
    @Provides
    fun provideAddressService(): AddressService {
        return Retrofit.Builder()
            .baseUrl("https://random-data-api.com/api/v2/")
            .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
            .client(client)
            .build()
            .create(AddressService::class.java)
    }

    @Singleton
    @Provides
    fun provideCountryMapper(): CountryDTOMapper {
        return CountryDTOMapper()
    }

    @Singleton
    @Provides
    fun provideCountryService(): CountryService {
        return Retrofit.Builder()
            .baseUrl("https://restcountries.com/v3.1/")
            .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
            .client(client)
            .build()
            .create(CountryService::class.java)
    }

}