package com.shahin.fmcountries.repository

import com.shahin.fmcountries.domain.model.Country

interface CountryRepository {

    suspend fun getCountry(countryName:String):Country
}