package com.shahin.fmcountries.repository

import com.shahin.fmcountries.domain.model.Country
import com.shahin.fmcountries.network.CountryService
import com.shahin.fmcountries.network.model.CountryDTOMapper

class CountryRepositoryImplementation(
    val countryService: CountryService,
    val mapper: CountryDTOMapper
):CountryRepository {
    override suspend fun getCountry(countryName: String): Country {
        val result = countryService.get(countryName)
        return mapper.toDomainList(result).first()
    }

}