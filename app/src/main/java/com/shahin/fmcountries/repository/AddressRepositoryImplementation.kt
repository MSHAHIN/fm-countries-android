package com.shahin.fmcountries.repository

import com.shahin.fmcountries.domain.model.Address
import com.shahin.fmcountries.network.AddressService
import com.shahin.fmcountries.network.model.AddressDtoMapper

class AddressRepositoryImplementation(
    private val addressService: AddressService,
    private val mapper: AddressDtoMapper):AddressRepository {

    override suspend fun getAddress(): Address {
        val result = addressService.get()
        return mapper.mapToDomainModel(result)
    }

}