package com.shahin.fmcountries.repository

import com.shahin.fmcountries.domain.model.Address


interface AddressRepository {

    suspend fun getAddress():Address

}